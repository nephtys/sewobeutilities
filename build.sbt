name := "SewobeUtilities"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "com.github.cb372" %% "scalacache-guava" % "0.9.2"

libraryDependencies += "com.lihaoyi" %% "upickle" % "0.4.3"

libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.3.0"