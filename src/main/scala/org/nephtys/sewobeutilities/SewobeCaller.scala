package org.nephtys.sewobeutilities

import upickle.default._

import scalaj.http.Http
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.Duration

/**
  * Created by nephtys on 10/16/16.
  */
object SewobeCaller {

  import scalacache._
  import guava._

  val cacheKey = "SEWOBEREST"

  private implicit val scalaCache = ScalaCache(GuavaCache())

  object async {
      def call()(implicit sewobeConfig: SewobeConfig, executionContext: ExecutionContext) : Future[IndexedSeq[Map[String, String]]] = {
        Future {
          sync.call()
        }
      }
    def cachedCall(timeToLive: Duration)(implicit sewobeConfig: SewobeConfig, executionContext: ExecutionContext) = cachingWithTTL(cacheKey)(timeToLive) {
        async.call()
    }
  }

  private case class DATENSATZCONTAINER (DATENSATZ: Map[String, String])

  object sync {
    def call()(implicit sewobeConfig: SewobeConfig) : IndexedSeq[Map[String, String]] = read[Map[String, DATENSATZCONTAINER]]({
      val resp = Http(sewobeConfig.sewobeURL).postForm(Seq("USERNAME" -> sewobeConfig.sewobeUsername,
        "PASSWORT" ->  sewobeConfig.sewobePassword,
        "AUSWERTUNG_ID" -> sewobeConfig.sewobeRequestID.toString)).asString
      if (resp.isSuccess) {
        resp.body
      } else {
        println(s"Sewobe REST call returned an error! Status line = ${resp.statusLine}")
        "{}"
      }
    }).values.map(_.DATENSATZ).toIndexedSeq

    def cachedCall(timeToLive: Duration)(implicit sewobeConfig: SewobeConfig) : IndexedSeq[Map[String, String]] =  {
      scalacache.sync.cachingWithTTL(cacheKey)(timeToLive){
        sync.call()
      }
    }

  }

}
