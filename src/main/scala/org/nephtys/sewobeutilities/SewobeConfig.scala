package org.nephtys.sewobeutilities

/**
  * Created by nephtys on 10/16/16.
  */
trait SewobeConfig {
  def sewobeURL : String
  def sewobeUsername : String
  def sewobePassword : String
  def sewobeRequestID : Int
}
